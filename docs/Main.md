# Patient BFF documentation
The patient-bff is a microservice intented for use in a microservice based system as the one described at 
[GMK - Architecture](https://bitbucket.org/4s/gmk-gravide-med-komplikationer/src/master/README.md). The service is a 
_backend-for-frontend service_ as described in [GMK Backend Architecture](https://bitbucket.org/4s/gmk-gravide-med-komplikationer/src/master/HFS.md) 
and provides a RESTful FHIR API for the citizens' app.

## Design
Instead of designing a single, general-purpose interface between client apps and various backend services,
you can choose to design separate APIs that handle the specific functionality provided by the different client 
applications. The principle of such separate interfaces for different client applications is called 
backend-for-frontend (BFF).

The client applications often have different needs to use the server's business functionality, and therefore it is 
often the case that the BFF is tightly coupled to a specific user experience. This principle is a good match for 
microservice architecture like ours, where backend services are loosly coupled and independently deployed. The BFF 
hides changes in the internal microservice infrastructure from the external clients, and provides a stable and 
dedicated business interface to these clients.

### Request handling
The service handles incoming REST requests by forwarding these requests to a downstream microsservice infrastructure.
Http GET requests will be forwarded through synchronous http GET requests to downstream microservices. Http requests 
that update state of downstream microservices are forwarded via messages to an underlying asynchronous messaging system 
(Apache Kafka). 
 
#### Read requests
When receiving a Http GET requests the services immediately sends required synchronous request to downstream 
services. It awaits for these requests to either succeed or fail, where failure includes timeout failures on 
downstream requests.
 
#### Updating requests
When receiving Http POST, PUT or DELETE requests the services will forward Create, Update or Delete commands via the
asynchronous messaging system. Before forwarding sending such a message to the messaging system the service will 
install a Future, telling a ReplyEventProcessor to listen for messages about success or failure of the particular 
command conveyed by the messages. The service will wait for the Future to return with such a reply before returning
a reply to the original incoming http POST, PUT or DELETE. In case waiting for the Future times out a timeout 
failure will be returned as a response to the original request. Notice that such a timout does not necessarily mean 
that the request didn't succceed in updating downstream services. It only means that we timed out before getting a 
response back. The downstream infrastructure is assumed to be idempotent.

#### "Optimistic locking" and ETAGs
The service and downstream infrastructure implements "optimistic locking" through the use of resource version 
information via the ETAG http header. See http://hl7.org/fhir/2018Sep/http.html#concurrency. This means that this 
service will return an ETAG http header with version of returned resources. All updating requests (POST, PUT, DELETE)
are required to use an `If-Match header that quotes the ETAG (with resource version) received from the server.

#### User context
All requests must have a security header that enables the service to lookup the employee that is performing the 
request. All requests are processed in this context, meaning that when for instance requesting all available patients
the query will respond with the patients assigned to the department of the employee currently logged in.