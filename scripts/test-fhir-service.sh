#!/bin/bash

#set -o errexit # exit script when command fails
#set -o pipefail # exit status of the last command that threw a non-zero exit code is returned
#set -o nounset # exit when script tries to use undeclared variables

function usage()
{
    echo "Test create and read on a FHIR service"
    echo
    echo "$(tput setaf 3)Usage:$(tput sgr 0)"
    echo "test-fhir-service.sh [-a|--address <service-address>] [--auth-server-url <auth-server-url>]"
    echo    "[-c|--capabilities <capabilities-list>] [-f|--fhir-version <fhir-version>] [-h|--help]"
    echo    "[-i|-ignore <capabilities-list>] [-k|-keycloak] [--keycloak-client-name <keycloak-client-name>]"
    echo    "[--keycloak-realm <keycloak-realm>] [-p|-password] [-r|--resource <resource-type>] [-s|--start-service]"
    echo    "[-t|--token <token>] [-u|--user <username>]"
    echo
    echo "$(tput setaf 3)Options:$(tput sgr 0)"
    echo
    echo "$(tput setaf 3)-a|--address <service-address>$(tput sgr 0)"
    echo "  Service endpoint, excluding resource address. E.g http://example.com/baseDstu3. If -a option is not used"
    echo "  the service is assumed to run on http://localhost:<port>/base<fhir-version> where port is read from"
    echo "  service.env file. <fhir-version> defaults to baseDstu3, see more under -f option."
    echo
    echo "$(tput setaf 3)--auth-server-url <auth-server-url>$(tput sgr 0)"
    echo "  Keycloak authentication server endpoint. Requires -k option. If not specified url is read from service.env"
    echo "  file."
    echo
    echo "$(tput setaf 3)-c|--capabilities <capabilities-list>$(tput sgr 0)"
    echo "  Comma separated list of http rest capabilities to test for. Supported values are POST and GET. If -c is not"
    echo "  specified both are tested. If only one of the two is specified it is also explicitely tested that the other"
    echo "  capability is not supported by the service."
    echo
    echo "$(tput setaf 3)-f|--fhir-version <fhir-version>$(tput sgr 0)"
    echo " FHIR version that the server is running. Supported values are Dstu3 and R4. Default is Dstu3."
    echo " FHIR version determines the base URL of the service, unless -a option is used."
    echo
    echo "$(tput setaf 3)-h|--help$(tput sgr 0)"
    echo "  Print this message."
    echo
    echo "$(tput setaf 3)-i|-ignore <capabilities-list>(tput sgr 0)"
    echo "  Comma separated list of http rest capabilities to ignore in test. Supported values are POST and GET. If -i"
    echo "  is not specified both are tested. If both -i and -c are specified -i overrides -c."
    echo
    echo "$(tput setaf 3)-k|-keycloak$(tput sgr 0)"
    echo "  When specified tests assume that the service supports authentication via keycloak. Requires at list -p and"
    echo "-u options as well."
    echo
    echo "$(tput setaf 3)--keycloak-client-name <keycloak-client-name>$(tput sgr 0)"
    echo "  The identification this service uses as client to Keycloak. If not specified this value is read from"
    echo "  service.env"
    echo
    echo "$(tput setaf 3)--keycloak-realm <keycloak-realm>$(tput sgr 0)"
    echo "  The Keycloak realm to login to"
    echo
    echo "$(tput setaf 3)-p|-password$(tput sgr 0)"
    echo "  Password to use when login in to Keycloak to get token"
    echo
    echo "$(tput setaf 3)-r|--resource <resource-type>$(tput sgr 0)"
    echo "  Resource type to test. Determines the resource part of the address to make requests against. Also"
    echo "  dertermines what file to load test resource from (will be loaded from"
    echo "  src/test/resources/<resource-type>.json"
    echo
    echo "$(tput setaf 3)-t|--token <token>$(tput sgr 0)"
    echo "  Security token used when making requests against service. This option can not be used in conjunction with"
    echo "  keycloak related options."
    echo
    echo "$(tput setaf 3)-s|--start-service$(tput sgr 0)"
    echo "  If specified the script will start the service using docker-compose"
    echo
    echo "$(tput setaf 3)-u|--user <username>$(tput sgr 0)"
    echo "  Username to use when login in to Keycloak to get token"
    echo
}

# Defaults:
STARTSERVICE=false
SUPPORT_POST=true
SUPPORT_GET=true
IGNORE_POST=false
IGNORE_GET=false
FHIR_VERSION="Dstu3"

# Read commandline arguments:
while :; do
    case $1 in
        -a|--address)
            if [ -n "$2" ]; then
                ADDRESS="$2"
                shift
            fi
            ;;
        --auth-server-url)
            if [ -n "$2" ]; then
                AUTH_SERVER_URL="$2"
                shift
            fi
            ;;
        -c|--capabilities)
            if [ -n "$2" ]; then
                SUPPORT_POST=false
                SUPPORT_GET=false
                capalist="$2"
                for i in ${capalist//,/ }
                do
                    if [[ $i = "POST" ]]; then
                        SUPPORT_POST=true
                    fi
                    if [[ $i = "GET" ]]; then
                        SUPPORT_GET=true
                    fi
                done
                shift
            fi
            ;;
        -f|--fhir-version)
            if [ -n "$2" ]; then
                FHIR_VERSION="$2"
                shift
            fi
            ;;
        -h|--help)
            usage
            exit 1
            ;;
         -i|--ignore)
            if [ -n "$2" ]; then
                capalist="$2"
                for i in ${capalist//,/ }
                do
                    if [[ $i = "POST" ]]; then
                        IGNORE_POST=true
                    fi
                    if [[ $i = "GET" ]]; then
                        IGNORE_GET=true
                    fi
                done
                shift
            fi
            ;;
        -k|--keycloak)
            ENABLEKEYCLOAK="true"
            ;;
        --keycloak-client-name)
            if [ -n "$2" ]; then
                KEYCLOAK_ClIENT_NAME="$2"
                shift
            fi
            ;;
        --keycloak-realm)
            if [ -n "$2" ]; then
                KEYCLOAK_REALM="$2"
                shift
            fi
            ;;
        -p|--password)
            if [ -n "$2" ]; then
                PASSWD="$2"
                shift
            fi
            ;;
        -r|--resource)
            if [ -n "$2" ]; then
                RESOURCE="$2"
                shift
            fi
            ;;
        -s|--start-service)
            STARTSERVICE=true
            if [ -n "$2" ]; then
                RESOURCE="$2"
                shift
            fi
            ;;
        -t|--token)
            if [ -n "$2" ]; then
                TOKEN="$2"
                shift
            fi
            ;;
        -u|--user)
            if [ -n "$2" ]; then
                USER="$2"
                shift
            fi
            ;;
        --)              # End of all options.
            shift
            break
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
            ;;
        *)               # Default case: No more options, so break out of the loop.
            break
    esac
    shift # past argument or value
done

echo


# We need a resource type...
if [ -z $RESOURCE ]; then
    echo ERROR FHIR resource type not specified. Please specify using -r option 1>&2
    exit 1
fi

echo SUPPORT_POST = "${SUPPORT_POST}"
echo SUPPORT_GET = "${SUPPORT_GET}"
echo
echo IGNORE_POST = $IGNORE_POST
echo IGNORE_GET = $IGNORE_GET
echo

# If host is not specified, use localhost
if [ -z $ADDRESS ]; then
    # Get port number for service (there must be a nicer way to do this...)
    PORT_LINES=$(grep -A1 'ports:' docker-compose.yml)
    echo PORT_LINES="${PORT_LINES}"
    while read -r PORT_LINES; do
        PORT_LINE="$PORT_LINES"
    done <<< "$PORT_LINES"
    echo PORT_LINE = "${PORT_LINE}"
    TMP=${PORT_LINE#- \"}
    echo TMP = $TMP
    PORT=${TMP%%\:*}
    echo PORT = "${PORT}"

    ADDRESS=http://localhost:${PORT}/base"${FHIR_VERSION}"
    echo ADDRESS="${ADDRESS}"
fi

echo

if [ -z $ENABLEKEYCLOAK ]; then
    echo "Not using Keycloak authentication"
else
    echo "Using Keycloak authentication"

    # Get settings from service.env file
    if [ -z $AUTH_SERVER_URL ]; then
        AUTH_SERVER_URL_LINE=$(grep  'AUTH_SERVER_URL=' service.env)
        echo AUTH_SERVER_URL_LINE = "${AUTH_SERVER_URL_LINE}"
        AUTH_SERVER_URL=${AUTH_SERVER_URL_LINE:16}
    fi
    echo AUTH_SERVER_URL = "${AUTH_SERVER_URL}"
    echo

    if [ -z $KEYCLOAK_ClIENT_NAME ]; then
        KEYCLOAK_ClIENT_NAME_LINE=$(grep 'KEYCLOAK_CLIENT_NAME' service.env)
        echo KEYCLOAK_ClIENT_NAME_LINE = "${KEYCLOAK_ClIENT_NAME_LINE}"
        KEYCLOAK_ClIENT_NAME=${KEYCLOAK_ClIENT_NAME_LINE:21}
    fi
    echo KEYCLOAK_ClIENT_NAME = "${KEYCLOAK_ClIENT_NAME}"

    if [ -z $KEYCLOAK_REALM ]; then
        KEYCLOAK_REALM_LINE=$(grep 'KEYCLOAK_REALM' service.env)
        echo KEYCLOAK_REALM_LINE = "${KEYCLOAK_REALM_LINE}"
        KEYCLOAK_REALM=${KEYCLOAK_REALM_LINE:15}
    fi
    echo KEYCLOAK_REALM = "${KEYCLOAK_REALM}"
    echo

    # Get access token from Keycloak
    echo Get access token from Keycloak:
    set -x
    OIDRESULT=$(curl --data "grant_type=password&client_id=${KEYCLOAK_ClIENT_NAME}&username=${USER}&password=${PASSWD}" ${AUTH_SERVER_URL}realms/${KEYCLOAK_REALM}/protocol/openid-connect/token)
    set -
    TOKEN=$(echo ${OIDRESULT} | sed 's/.*access_token":"//g' | sed 's/".*//g')
    echo TOKEN = $TOKEN
fi

echo
echo

if [ $STARTSERVICE = true ]; then
    # Start service using docker-compose
    echo "Starting service using docker-compose"
    ( set -x; docker-compose up -d )
    echo
    # Wait for service to return status 200 on curl (timeout after 5 retries, with 10 sec timeout on curl)
    echo "Waiting for service"
    RETRIES=0
    RESULT=0
    until [[ $RESULT = 200 ]] || [[ $RETRIES -gt 20 ]]; do
        printf '.'
        sleep 5
        if [ -z $TOKEN ]; then
            set -x
            RESULT=$(curl -m 10 -o /dev/null --silent --write-out '%{http_code}\n' ${ADDRESS}/metadata)
            set -
        else
            set -x
            RESULT=$(curl -H "Authorization: Bearer $TOKEN" -m 10 -o /dev/null --silent --write-out '%{http_code}\n' ${ADDRESS}/metadata)
            set -
        fi
        ((RETRIES++))
    done
fi

echo
echo "Running tests..."
echo

###################################################################
# GET test

if [ "$IGNORE_GET" = true ]; then
    echo "Ignoring GET test!"
else
    echo ============= GET test =============
    echo
    if [ -z $TOKEN ]; then
        set -x
        RESULT=$(curl -X GET --header "Content-Type:application/json" ${ADDRESS}/${RESOURCE})
        set -
    else
        set -x
        RESULT=$(curl -X GET -H "Authorization: Bearer $TOKEN" --header "Content-Type:application/json" ${ADDRESS}/${RESOURCE})
        set -
    fi
    echo
    echo SUPPORT_GET = $SUPPORT_GET
    if [ $SUPPORT_GET = true ]; then
      if [[ $RESULT =~ 'searchset' ]]; then
        GET_TEST_RES=true
      else
        GET_TEST_RES=false
      fi
    else
      if [[ $RESULT =~ 'searchset' ]]; then
        GET_TEST_RES=false
      else
        GET_TEST_RES=true
      fi
    fi
    echo GET_TEST_RES = $GET_TEST_RES
    if [ $GET_TEST_RES = true ]; then
        echo "$(tput setaf 2)Success: Test GET.$(tput sgr 0)"
    else
        echo "$(tput setaf 1)Failure: Test GET.$(tput sgr 0)"
    fi
fi
echo

###################################################################
# POST test

if [ "$IGNORE_POST" = true ]; then
    echo "Ignoring POST test!"
else
    echo ============= POST test =============
    echo

    # Notice assumption in "--data" part of curl call about location and name of json file
    if [ -z $TOKEN ]; then
        set -x
        RESULT=$(curl --request POST --url ${ADDRESS}/${RESOURCE} \
            --header 'cache-control: no-cache' \
            --header 'content-type: application/json' \
            --data "@src/test/resources/${RESOURCE}.json")
        set -
    else
        set -x
        RESULT=$(curl --request POST -H "Authorization: Bearer $TOKEN" --url ${ADDRESS}/${RESOURCE} \
            --header 'cache-control: no-cache' \
            --header 'content-type: application/json' \
            --data "@src/test/resources/${RESOURCE}.json")
        set -
    fi
    echo
    echo SUPPORT_POST = $SUPPORT_POST
    if [ $SUPPORT_POST = true ]; then
        if [[ $RESULT =~ 'Successfully' ]]; then
          POST_TEST_RES=true
        else
          POST_TEST_RES=false
        fi
    else
        if [[ $RESULT =~ 'Successfully' ]]; then
          POST_TEST_RES=false
        else
          POST_TEST_RES=true
        fi
    fi
    echo POST_TEST_RES = $POST_TEST_RES
    if [ $POST_TEST_RES = true ]; then
      echo "$(tput setaf 2)Success: Test POST.$(tput sgr 0)"
    else
      echo "$(tput setaf 1)Failure: Test POST.$(tput sgr 0)"
    fi
fi

###################################################################
# Stop the services again (if we were asked to start them)

if [ $STARTSERVICE = true ]; then
    ( set -x; docker-compose down )
fi

###################################################################
# Summary

echo
echo ============= Summary =============

if [ "$IGNORE_GET" = false ]; then
    if [ $GET_TEST_RES = true ]; then
        echo "$(tput setaf 2)Testing GET resource succeeded (Testing for GET capability = $SUPPORT_GET) $(tput sgr 0)"
    else
        echo "$(tput setaf 1)Testing GET resource failed. (Testing for GET capability = $SUPPORT_GET)$(tput sgr 0)"
    fi
fi

if [ "$IGNORE_POST" = false ]; then
    if [ $POST_TEST_RES = true ]; then
        echo "$(tput setaf 2)Testing POST resource succeeded (Testing for POST capability = $SUPPORT_POST) $(tput sgr 0)"
    else
        echo "$(tput setaf 1)Testing POST resource failed (Testing for POST capability = $SUPPORT_POST)$(tput sgr 0)"
    fi
fi