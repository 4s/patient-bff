#!/bin/bash
#########################
# 4S Deploy script
#
# jesper.jakobsen@alexandra.dk

LOG=~/logs/deploy.log

display_usage() {
  echo "Usage: $0 [IMAGE-REPO] [IMAGE:TAG] [SERVICE NAME] [DEPLOYMENT NAME] [GIT-REVISION]"
}

touch_log_file() { 
  mkdir -p "$(dirname "$LOG")" || return; touch "$LOG";
}

log() {
  echo $(date)":" $* >> ${LOG}; tail -n1 ${LOG}
}

die() {
  log "Error during deployment, check logfile: $LOG"
  exit 2
}

perform() {
  log "$1"
  eval "$1" >> ${LOG} || die
}

if [[  $# -le 4 ]]; then
  display_usage
  exit 1
fi

REPO=$1
IMAGE_NAME=$2
SERVICE_NAME=$3
DEPLOYMENT_NAME=$4
REVISION=$5

touch_log_file
echo "" >> ${LOG}
log "DEPLOY BEGIN: repo=$REPO, image=$IMAGE_NAME, service=$SERVICE_NAME, deployment=$DEPLOYMENT_NAME, revision=$REVISION"
perform "cd ~/src/$SERVICE_NAME"

# Update local branch
perform "git fetch origin"
perform "git reset --hard $REVISION"
perform "git submodule update --init"
perform "git submodule foreach git pull origin master"

# Pull image
perform "docker login -u dockerpuller -p OophuePhegohz2ae $REPO"
perform "docker pull $REPO/$IMAGE_NAME"

#Update service
perform "cp ~/ms-scripts/$DEPLOYMENT_NAME/deploy.env ./"
perform "printf 'version: \"3.4\"\nservices:\n  $SERVICE_NAME:\n    image: $REPO/$IMAGE_NAME\n' > docker-compose.deploy.yml"
perform "docker-compose -f docker-compose.yml -f docker-compose.debug.yml -f docker-compose.fluentd-logging.yml -f docker-compose.deploy.yml up -d"

log "DEPLOY END"

