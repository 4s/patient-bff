#!/bin/bash

#set -o errexit # exit script when command fails
#set -o pipefail # exit status of the last command that threw a non-zero exit code is returned
#set -o nounset # exit when script tries to use undeclared variables

function usage()
{
    echo "Maven package and docker build project"
    echo
    echo "$(tput setaf 3)Usage:$(tput sgr 0)"
    echo "buildall.sh [-n|--name <name>] [-r|--repository <repository>] [-t|--tag <tag>] [-k|-keycloak] [-h|help]"
    echo
    echo "$(tput setaf 3)Options:$(tput sgr 0)"
    echo
    echo "$(tput setaf 3)-n|--name <name>$(tput sgr 0)"
    echo "  Name of docker image to build. If not specified the name will be picked up from Maven POM file."
    echo
    echo "$(tput setaf 3)-r|--repository <repository>$(tput sgr 0)"
    echo "  Target docker repository. If not specified a local image will be built."
    echo
    echo "$(tput setaf 3)-t|--tag <tag>$(tput sgr 0)"
    echo "  Tag for docker image. If not specified version number will be picked up from Maven POM file and image will"
    echo "  be tagged with version number and \"latest\""
    echo
    echo "$(tput setaf 3)-k|-keycloak$(tput sgr 0)"
    echo "  Build service with Keycloak security."
    echo
    echo "$(tput setaf 3)[-h|help]$(tput sgr 0)"
    echo "  Print this message."
    echo
}

# Read commandline arguments:
while :; do
    case $1 in
        -h|--help)
            usage
            exit 1
            ;;
        -k|--keycloak)
            ENABLEKEYCLOAK="true"
            ;;
        -n|--name)
            if [ -n "$2" ]; then
                NAME="$2"
                shift
            fi
            ;;
        -r|--repository)
            if [ -n "$2" ]; then
                REPOSITORY="$2"
                shift
            fi
            ;;
        -t|--tag)
            if [ -n "$2" ]; then
                TAG="$2"
                shift
            fi
            ;;
        --)              # End of all options.
            shift
            break
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
            ;;
        *)               # Default case: No more options, so break out of the loop.
            break
    esac
    shift # past argument or value
done

echo NAME  = "${NAME}"
echo TAG     = "${TAG}"
echo REPOSITORY = "${REPOSITORY}"
echo ENABLEKEYCLOAK = ${ENABLEKEYCLOAK}

#if [[ -n $1 ]]; then
#    echo "Last line of file specified as non-opt/last argument:"
#    tail -1 $1
#fi

# If name not specified, get name from pom:
if [ -z $NAME ]; then
    echo "Getting artifactId from pom..."
    NAME=$(mvn -q -Dexec.executable="echo" -Dexec.args='${project.artifactId}' --non-recursive exec:exec)
    echo artifactId = "${NAME}"
fi

# If tag not specified, get tag from pom:
if [ -z $TAG ]; then
    echo "Getting version from pom..."
    TAG=$(mvn -q -Dexec.executable="echo" -Dexec.args='${project.version}' --non-recursive exec:exec)
    echo version = "${TAG}"
fi

# Maven build, docker package and push to repository:
if [ -z $ENABLEKEYCLOAK ]; then
    ( set -x; mvn -DskipTests clean package )
elif [ "$ENABLEKEYCLOAK" == "true" ]; then
    ( set -x; mvn -DskipTests clean package -P withKeycloak)
else
    ( set -x; mvn -DskipTests clean package)
fi

if [ -z $REPOSITORY ]; then
    echo "No repository specified, building local image"
    ( set -x; docker build -t $NAME:latest -t $NAME:$TAG . ) #no repository => build locally, no docker push
else
    ( set -x; docker build -t ${REPOSITORY}/$NAME:latest -t ${REPOSITORY}/$NAME:$TAG . )
    ( set -x; docker push ${REPOSITORY}/$NAME:$TAG )
fi
