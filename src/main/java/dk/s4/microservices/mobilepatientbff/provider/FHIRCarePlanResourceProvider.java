package dk.s4.microservices.mobilepatientbff.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.exceptions.FhirClientConnectionException;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.exceptions.AuthenticationException;
import dk.s4.microservices.microservicecommon.fhir.ExceptionUtils;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import dk.s4.microservices.mobilepatientbff.ServiceVariables;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.CarePlan;
import org.hl7.fhir.r4.model.Identifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FHIRCarePlanResourceProvider implements IResourceProvider {

    private static final Logger logger = LoggerFactory.getLogger(FHIRCarePlanResourceProvider.class);
    private final FhirContext context;
    private UserContextResolverInterface userContextResolver;
    private MyFhirClientFactory clientFactory;

    public FHIRCarePlanResourceProvider(FhirContext context,
                                           UserContextResolverInterface userContextResolver,
                                           MyFhirClientFactory clientFactory) {
        this.context = context;
        this.userContextResolver = userContextResolver;
        this.clientFactory = clientFactory;
    }

    /**
     * The getResourceType method comes from IResourceProvider, and must be
     * overridden to indicate what type of resource this provider supplies.
     * @return
     */
    @Override
    public Class<CarePlan> getResourceType() {
        return CarePlan.class;
    }

    /**
     * Returns a FHIR Bundle of active FHIR CarePlan resources, pertaining to the currently logged in patient.
     * @param request the request
     * @param response the reponse
     * @return A Bundle of FHIR CarePlans
     */

    @Search(queryName = "getMyActive")
    public Bundle getMyActive(HttpServletRequest request, HttpServletResponse response) {
        try{
            logger.debug("getMyActive");
            Identifier identifier = null;
            try {
                identifier = userContextResolver.getFHIRUserId(request).setSystem(ServiceVariables.OFFICIAL_PATIENT_IDENTIFIER_SYSTEM.getValue());
            } catch (UserContextResolverInterface.UserContextResolverException e) {
                logger.error(e.getMessage(), e);
                throw new AuthenticationException(e.getMessage(), e);
            }
            TokenParam identifierToken = new TokenParam().setSystem(identifier.getSystem()).setValue(identifier.getValue());


            String hostname = ServiceVariables.PATIENTCARE_SERVICE_URL.getValue();
            IGenericClient client = clientFactory.getClientFromBaseUrl(ServiceVariables.PATIENTCARE_SERVICE_URL.getValue(), context);
            String searchUrl = hostname + "/CarePlan?_query=getActiveForPatient&identifier="+identifierToken.getValueAsQueryToken(context);

            return client.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();

        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        }
    }

}
