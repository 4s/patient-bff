package dk.s4.microservices.mobilepatientbff.provider;


import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.annotation.RequiredParam;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.exceptions.FhirClientConnectionException;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.exceptions.AuthenticationException;
import ca.uhn.fhir.rest.server.exceptions.InternalErrorException;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import dk.s4.microservices.microservicecommon.fhir.ExceptionUtils;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface.UserContextResolverException;
import dk.s4.microservices.mobilepatientbff.ServiceVariables;
import org.hl7.fhir.r4.model.*;
import org.hl7.fhir.r4.model.CarePlan.CarePlanActivityComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class that represents a resource provider for the FHIR Questionnaire
 * resource, which supports the search operation.
 */
public class FHIRQuestionnaireResourceProvider implements IResourceProvider {

	private static final Logger logger = LoggerFactory.getLogger(FHIRQuestionnaireResourceProvider.class);
	private final FhirContext context;
	private UserContextResolverInterface userContextResolver;
	private MyFhirClientFactory clientFactory;

	public FHIRQuestionnaireResourceProvider(FhirContext context,
			UserContextResolverInterface userContextResolver,
			MyFhirClientFactory clientFactory) {
		this.context = context;
		this.userContextResolver = userContextResolver;
		this.clientFactory = clientFactory;
	}

	/**
	 * Returns the type of resource this provider provides.
	 */
	@Override
	public Class<Questionnaire> getResourceType() {
		return Questionnaire.class;
	}

	/**
	 * Returns a FHIR Bundle of FHIR Questionnaire resources, that have been assgined to the patient.
     *
     * The patient ID is assumed to be embedded in a security token that the caller should transfer via the security
	 * header of the http request.
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http response
	 *
	 * @return A Bundle of FHIR Questionnaire resources assigned to the patient.
	 *
	 * @throws ResourceNotFoundException if no active CarePlan can be found for the patient or if CarePlan doesn't have
	 * any activities.
	 * @throws InternalErrorException if more than one active CarePlan is found for the patient.
	 */
	@Search(queryName = "getMyQuestionnaires")
	public Bundle getMyQuestionnaires(HttpServletRequest theRequest, HttpServletResponse theResponse) {
		try {
			logger.debug("getMyQuestionnaires");
			Identifier identifier = null;
			try {
				identifier = userContextResolver.getFHIRUserId(theRequest).setSystem(ServiceVariables.OFFICIAL_PATIENT_IDENTIFIER_SYSTEM.getValue());
			} catch (UserContextResolverException e) {
				logger.error(e.getMessage(), e);
				throw new AuthenticationException(e.getMessage(), e);
			}
			TokenParam identifierToken = new TokenParam().setSystem(identifier.getSystem()).setValue(identifier.getValue());

			//Query paitentcare-service for active CarePlan
			IGenericClient patientCareClient = clientFactory.getClientFromBaseUrl(ServiceVariables.PATIENTCARE_SERVICE_URL.getValue(), context);
			String patientCareQueryUrl = ServiceVariables.PATIENTCARE_SERVICE_URL.getValue()
					+ "/CarePlan?_query=getActiveForPatient&identifier="
					+ identifierToken.getValueAsQueryToken(context);
			Bundle activeCarePlans = patientCareClient.search().byUrl(patientCareQueryUrl).returnBundle(Bundle.class).execute();

			return extractQuestionnairesToBundle(activeCarePlans);
		} catch (FhirClientConnectionException e) {
			throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
		}
	}

	protected Bundle extractQuestionnairesToBundle(Bundle activeCarePlans){
		Bundle resultingBundle = new Bundle();
		for(Bundle.BundleEntryComponent component : activeCarePlans.getEntry()){
			CarePlan carePlan = (CarePlan) component.getResource();
			if (!carePlan.hasActivity())
				throw new ResourceNotFoundException("No actives in CarePlan");
			for (CarePlanActivityComponent activity : carePlan.getActivity()) {
				Task task = getActivityTask(activity);
				if (task == null)
					continue;
				CanonicalType questionnaireCanonical = getTaskInputCanonical(task);
				if (questionnaireCanonical == null)
					continue;
				Questionnaire questionnaire = getQuestionnaireByCanonical(questionnaireCanonical);
				if (questionnaire == null)
					continue;
				resultingBundle.addEntry().setResource(questionnaire);
			}
		}
		return resultingBundle;
	}

	@Search(queryName = "getForCarePlan")
	public Bundle getForCarePlan(HttpServletRequest theRequest, HttpServletResponse theResponse,
										  @RequiredParam(name = CarePlan.SP_IDENTIFIER)
										  TokenParam carePlanIdentifier){
		try {
			IGenericClient patientCareClient = clientFactory.getClientFromBaseUrl(ServiceVariables.PATIENTCARE_SERVICE_URL.getValue(), context);

			String searchUrl = ServiceVariables.PATIENTCARE_SERVICE_URL.getValue() + "/CarePlan?identifier=" +
					carePlanIdentifier.getValueAsQueryToken(context);
			Bundle carePlanBundle = patientCareClient.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
			if (carePlanBundle == null || carePlanBundle.isEmpty())
				throw new ResourceNotFoundException("No active CarePlan found with this identifier: " + carePlanIdentifier.getValueAsQueryToken(context));
			if (carePlanBundle.getEntry().size() > 1)
				throw new InternalErrorException("More than one CarePlan matched this identifier:" + carePlanIdentifier.getValueAsQueryToken(context));

			CarePlan carePlan = (CarePlan) carePlanBundle.getEntry().get(0).getResource();
			Bundle resultingBundle = new Bundle();
			if (!carePlan.hasActivity())
				throw new ResourceNotFoundException("No actives in CarePlan");
			for (CarePlanActivityComponent activity : carePlan.getActivity()) {
				Task task = getActivityTask(activity);
				if (task == null)
					continue;
				CanonicalType questionnaireCanonical = getTaskInputCanonical(task);
				if (questionnaireCanonical == null)
					continue;
				Questionnaire questionnaire = getQuestionnaireByCanonical(questionnaireCanonical);
				if (questionnaire == null)
					continue;
				resultingBundle.addEntry().setResource(questionnaire);
			}

			return resultingBundle;

		} catch (FhirClientConnectionException e) {
			throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
		}
	}

	private Task getActivityTask(CarePlanActivityComponent activity) {
		if (activity.hasReference() && activity.getReference().hasType() && activity.getReference().getType().equals("Task"))
			return (Task) activity.getReference().getResource();

		return null;
	}

	private CanonicalType getTaskInputCanonical(Task task) {
		if (task.hasInput() && task.getInput().get(0).hasType()) {
			CodeableConcept inputType = task.getInput().get(0).getType();
			if (inputType.hasCoding() && inputType.getCoding().get(0).hasCode()
					&& inputType.getCoding().get(0).getCode().equals("Questionnaire")) {
				Type inputValue = task.getInput().get(0).getValue();
				if (inputValue instanceof CanonicalType)
					return (CanonicalType) inputValue;
			}
		}

		return null;
	}

	private Questionnaire getQuestionnaireByCanonical(CanonicalType canonical) {
		IGenericClient outcomedefClient = clientFactory.getClientFromBaseUrl(ServiceVariables.OUTCOMEDEFINITION_SERVICE_URL.getValue(), context);
		String canonicalString = canonical.getValue();
		if (canonicalString.isEmpty())
			return null;
		String[] splitCanonical = canonicalString.split("\\|");
		String searchUrl = ServiceVariables.OUTCOMEDEFINITION_SERVICE_URL.getValue()
				+ "/Questionnaire?url=" + splitCanonical[0]
				+ (splitCanonical.length == 2 ? "&version=" + splitCanonical[1] : "");
		Bundle questionnaires = outcomedefClient.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
		if (questionnaires.hasEntry())
			return (Questionnaire) questionnaires.getEntry().get(0).getResource();

		return null;
	}
}
