package dk.s4.microservices.mobilepatientbff.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.annotation.OptionalParam;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.exceptions.FhirClientConnectionException;
import ca.uhn.fhir.rest.param.DateParam;
import ca.uhn.fhir.rest.param.DateRangeParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.exceptions.AuthenticationException;
import dk.s4.microservices.microservicecommon.fhir.ExceptionUtils;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface.UserContextResolverException;
import dk.s4.microservices.mobilepatientbff.ServiceVariables;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Observation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that represents a resource provider for the FHIR Observation resource,
 * which supports the search operation.
 */
public class FHIRObservationResourceProvider implements IResourceProvider {

    private static final Logger logger = LoggerFactory.getLogger(FHIRObservationResourceProvider.class);
    private final FhirContext context;
    private UserContextResolverInterface userContextResolver;
    private MyFhirClientFactory clientFactory;

    public FHIRObservationResourceProvider(FhirContext context,
            UserContextResolverInterface userContextResolver,
            MyFhirClientFactory clientFactory) {
        this.context = context;
        this.userContextResolver = userContextResolver;
        this.clientFactory = clientFactory;
    }

    /**
     * The getResourceType method comes from IResourceProvider, and must be
     * overridden to indicate what type of resource this provider supplies.
     */
    @Override
    public Class<Observation> getResourceType() {
        return Observation.class;
    }


    /**
     * Returns a FHIR Bundle of FHIR Observation resources that belongs to the patient.
     * <p>
     * Two optional search parameters can be provided to narrow the search: date and the type of observation.
     * <p>
     * The patient ID is assumed to be embedded in a security token that the caller should transfer via the security header of the http request.
     * <p>
     * Observations of type CTG measurements are not returned.
     *
     * @param theRequest  Incoming http request
     * @param theResponse Outgoing http response
     * @param date        Time interval in which the observations have been created. (Optional)
     * @param code        The code of the observation type. (Optional)
     * @return A Bundle of FHIR Observation resources assigned to the
     * patient.
     */
    @Search(queryName = "getMyObservations")
    public Bundle getMyObservations(HttpServletRequest theRequest, HttpServletResponse theResponse,
                         @OptionalParam(name = Observation.SP_DATE) DateRangeParam date,
                         @OptionalParam(name = Observation.SP_CODE) TokenParam code) {
        try {
            logger.debug("getMyObservations");

            Identifier identifier = null;
            try {
                identifier = userContextResolver.getFHIRUserId(theRequest).setSystem(ServiceVariables.OFFICIAL_PATIENT_IDENTIFIER_SYSTEM.getValue());
            } catch (UserContextResolverException e) {
                logger.error(e.getMessage(), e);
                throw new AuthenticationException(e.getMessage(), e);
            }
            TokenParam identifierToken = new TokenParam().setSystem(identifier.getSystem()).setValue(identifier.getValue());

            IGenericClient client = clientFactory.getClientFromBaseUrl(outcomeServiceUrl(), context);
            StringBuilder builder = new StringBuilder(outcomeServiceUrl() + "/Observation?_query=searchBySubjectIdentifier&subject-identifier=" + identifierToken.getValueAsQueryToken(context));
            if (date != null && !date.isEmpty()) {
                for (DateParam dateParam : date.getValuesAsQueryTokens()) {
                    builder.append("&");
                    builder.append(Observation.SP_DATE);
                    builder.append("=");
                    builder.append(dateParam.getValueAsQueryToken(context));
                }
            }
            if (code != null) {
                builder.append("&");
                builder.append(Observation.SP_CODE);
                builder.append("=");
                builder.append(code.getValueAsQueryToken(context));
            }
            return client.search().byUrl(builder.toString()).returnBundle(Bundle.class).execute();
        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        }
    }

    private String outcomeServiceUrl() {
        return ServiceVariables.OUTCOME_SERVICE_URL.getValue();
    }
}
