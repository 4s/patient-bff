package dk.s4.microservices.mobilepatientbff.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.exceptions.FhirClientConnectionException;
import ca.uhn.fhir.rest.param.DateParam;
import ca.uhn.fhir.rest.param.DateRangeParam;
import ca.uhn.fhir.rest.param.ReferenceParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.exceptions.AuthenticationException;
import ca.uhn.fhir.rest.server.exceptions.NotImplementedOperationException;
import dk.s4.microservices.messaging.ReplyEventProcessor;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.kafka.KafkaEventProducer;
import dk.s4.microservices.microservicecommon.fhir.ExceptionUtils;
import dk.s4.microservices.microservicecommon.fhir.FhirBaseBFFResourceProvider;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface.UserContextResolverException;
import dk.s4.microservices.mobilepatientbff.ServiceVariables;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FHIRTaskResourceProvider extends FhirBaseBFFResourceProvider {

    private static final Logger logger = LoggerFactory.getLogger(FHIRTaskResourceProvider.class);

    public FHIRTaskResourceProvider(FhirContext ctx,
                                    UserContextResolverInterface userContextResolver,
                                    MyFhirClientFactory clientFactory,
                                    KafkaEventProducer kafkaEventProducer,
                                    ReplyEventProcessor replyEventProcessor) {
        super(ctx, userContextResolver, clientFactory, kafkaEventProducer, replyEventProcessor);
    }

    @Override
    protected Identifier identifierForResource(IBaseResource resource) {
        return ((Task) resource).getIdentifier().stream()
                .filter(identifier -> identifier.getSystem().equals(ServiceVariables.OFFICIAL_TASK_IDENTIFIER_SYSTEM.getValue()))
                .findAny()
                .orElse(null);
    }

    /**
     * Updates a task resource on the OutcomeService-service and returns a MethodOutcome resource indicating whether the
     * action was successful.
     *
     * @param theRequest The incoming request
     * @param theResponse The outgoing response
     * @param theTask The updated task to be persisted on the Outcome-service
     *
     * @return A MethodOutcome resource indicating whether the action was successful.
     */
    @Update
    public MethodOutcome updateTask(HttpServletRequest theRequest,
                                    HttpServletResponse theResponse,
                                    @ResourceParam Task theTask) {
        logger.debug("Update Task");
        return createOrUpdate(theRequest, theResponse, theTask, Topic.Operation.Update);
    }

    /**
     * Returns the type of resource this provider provides.
     */
    @Override
    public Class<Task> getResourceType() {
        return Task.class;
    }

    /**
     * Returns a FHIR Bundle of FHIR Task resources containing notifications,
     * that a patient may receive when a questionnaire has been modified by a clinician.
     * <p>
     * Poll this method, when questionnaires are displayed to the patient and there exist notifications about modifications to
     * questionnaires.
     *
     * @param theRequest  Incoming http request
     * @param theResponse Outgoing http response
     * @param code        TaskType code. Here the Task.code must contain the value: 'acknowledgement'. (Required)
     * @param reasonCode  TaskReason code. A description or code indicating why this task needs to be performed. Of type CodeableConcept and must contain the value: 'information-updated'. (Required)
     * @param focus       What the task is acting on. Must be a logical reference to a Questionnaire. (Required)
     * @return Bundle of FHIR Task resources representing notification(s)
     * @see <a href="https://issuetracker4s.atlassian.net/wiki/spaces/FDM/pages/47251463/FHIR+Task">FHIR Task</a>
     */
    @Search(queryName = "searchNotifications")
    public Bundle searchNotifications(HttpServletRequest theRequest, HttpServletResponse theResponse,
                                      @RequiredParam(name = Task.SP_CODE) TokenParam code,
                                      @RequiredParam(name = "reasonCode") TokenParam reasonCode,
                                      @RequiredParam(name = Task.SP_FOCUS) ReferenceParam focus) {

        logger.debug("searchNotifications (notification about questionnaire update)");

        // Note search for Task resources, where Task.status contains the value 'requested'

        throw new NotImplementedOperationException("This operations have not been implemented yet");

//        try {
//
//        } catch (FhirClientConnectionException e) {
//            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
//        }
    }

    /**
     * Gets a FHIR Task resource containing an acknowledgement.
     *
     * @param identifier The identifier of the Task. Is used in the search method. (Required)
     * @return A FHIR Task resource representing an acknowledgement
     * @see <a href="https://issuetracker4s.atlassian.net/wiki/spaces/FDM/pages/47251463/FHIR+Task">FHIR Task</a>
     */
    @Search(queryName = "getByIdentifier")
    public Task getByIdentifier(@RequiredParam(name = Task.SP_IDENTIFIER) TokenParam identifier) {
        logger.debug("getByIdentifier");

        throw new NotImplementedOperationException("This operations have not been implemented yet");

        //        try {
//
//        } catch (FhirClientConnectionException e) {
//            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
//        }
    }

    /**
     * Returns a bundle containing all tasks matching the code, for the currently logged in patient.
     * If a DateRangeParam is specified, only tasks matching the period will be returned.
     *
     * @param theRequest  Incoming http request
     * @param theResponse Outgoing http response
     * @param code        The type of task = (acknowledgement, review, activity)
     * @param authored_on Period which the creation date of the Task should be within
     * @return Bundle of Tasks
     */
    @Search(queryName = "getTaskForPatientWithCode")
    public Bundle getTaskForPatientWithCode(HttpServletRequest theRequest, HttpServletResponse theResponse,
                                            @RequiredParam(name = Task.SP_CODE) TokenParam code,
                                            @OptionalParam(name = Task.SP_AUTHORED_ON) DateRangeParam authored_on) {
        try {
            logger.debug("getTaskForPatientWithCode");

            Identifier patientIdentifier = null;
            try {
                patientIdentifier = userContextResolver.getFHIRUserId(theRequest).setSystem(ServiceVariables.OFFICIAL_PATIENT_IDENTIFIER_SYSTEM.getValue());
            } catch (UserContextResolverException e) {
                logger.error(e.getMessage(), e);
                throw new AuthenticationException(e.getMessage(), e);
            }
            TokenParam identifierToken = new TokenParam().setSystem(patientIdentifier.getSystem()).setValue(patientIdentifier.getValue());

            IGenericClient client = clientFactory.getClientFromBaseUrl(ServiceVariables.OUTCOME_SERVICE_URL.getValue(), fhirContext);
            StringBuilder builder = new StringBuilder(ServiceVariables.OUTCOME_SERVICE_URL.getValue() + "/Task?_query=getTaskForPatientWithCode&code=" +
                                                              code.getValue() + "&identifier=" + identifierToken.getValueAsQueryToken(fhirContext));
            if (authored_on != null) {
                // Search for tasks that have been authored in given period
                for (DateParam dateParam : authored_on.getValuesAsQueryTokens()) {
                    builder.append("&");
                    builder.append(Task.SP_AUTHORED_ON);
                    builder.append("=");
                    builder.append(dateParam.getValueAsQueryToken(fhirContext));
                }
            }
            return client.search().byUrl(builder.toString()).returnBundle(Bundle.class).execute();
        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        }
    }

    /**
     * Returns a bundle containing all tasks containing the acknowledgement code, for the currently logged in patient.
     * If a DateRangeParam is specified, only tasks matching the period will be returned.
     *
     * @param theRequest  Incoming http request
     * @param theResponse Outgoing http response
     * @param authored_on Period which the creation date of the Task should be within
     * @return Bundle of Tasks
     */
    @Search(queryName = "getAcknowledgementTasks")
    public Bundle getAcknowledgementTasks(HttpServletRequest theRequest, HttpServletResponse theResponse,
                                          @OptionalParam(name = Task.SP_AUTHORED_ON) DateRangeParam authored_on) {
        try {
            logger.debug("getTaskForPatientWithCode");

            Identifier patientIdentifier = null;
            try {
                patientIdentifier = userContextResolver.getFHIRUserId(theRequest).setSystem(ServiceVariables.OFFICIAL_PATIENT_IDENTIFIER_SYSTEM.getValue());
            } catch (UserContextResolverException e) {
                logger.error(e.getMessage(), e);
                throw new AuthenticationException(e.getMessage(), e);
            }
            TokenParam identifierToken = new TokenParam().setSystem(patientIdentifier.getSystem()).setValue(patientIdentifier.getValue());

            IGenericClient client = clientFactory.getClientFromBaseUrl(ServiceVariables.OUTCOME_SERVICE_URL.getValue(), fhirContext);
            StringBuilder builder = new StringBuilder(ServiceVariables.OUTCOME_SERVICE_URL.getValue() + "/Task?_query=getTaskForPatientWithCode" +
                                                              "&code=https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100794407/Task%2btype%2bcoding%2bsystem|acknowledgement" +
                                                              "&identifier=" + identifierToken.getValueAsQueryToken(fhirContext));
            if (authored_on != null) {
                // Search for tasks that have been authored in given period
                for (DateParam dateParam : authored_on.getValuesAsQueryTokens()) {
                    builder.append("&");
                    builder.append(Task.SP_AUTHORED_ON);
                    builder.append("=");
                    builder.append(dateParam.getValueAsQueryToken(fhirContext));
                }
            }
            return client.search().byUrl(builder.toString()).returnBundle(Bundle.class).execute();
        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        }
    }
}

