package dk.s4.microservices.mobilepatientbff.servlet;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.StrictErrorHandler;
import ca.uhn.fhir.rest.api.EncodingEnum;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.RestfulServer;
import ca.uhn.fhir.rest.server.interceptor.CorsInterceptor;
import ca.uhn.fhir.rest.server.interceptor.LoggingInterceptor;
import ca.uhn.fhir.rest.server.interceptor.ResponseHighlighterInterceptor;
import dk.s4.microservices.messaging.MessagingInitializationException;
import dk.s4.microservices.messaging.ReplyEventProcessor;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.kafka.KafkaConsumeAndProcess;
import dk.s4.microservices.messaging.kafka.KafkaEventProducer;
import dk.s4.microservices.messaging.kafka.KafkaInitializationException;
import dk.s4.microservices.microservicecommon.FhirTopics;
import dk.s4.microservices.microservicecommon.fhir.DiasInterceptorAdaptor;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.fhir.MyInterceptorAdaptor;
import dk.s4.microservices.microservicecommon.security.*;
import dk.s4.microservices.mobilepatientbff.NullUserContextResolver;
import dk.s4.microservices.mobilepatientbff.ServiceVariables;
import dk.s4.microservices.mobilepatientbff.auditlog.DiasAuditLogInterceptorAdaptor;
import dk.s4.microservices.mobilepatientbff.auditlog.DiasSender;
import dk.s4.microservices.mobilepatientbff.health.HealthEndpoint;
import dk.s4.microservices.mobilepatientbff.provider.FHIRCarePlanResourceProvider;
import dk.s4.microservices.mobilepatientbff.provider.FHIRObservationResourceProvider;
import dk.s4.microservices.mobilepatientbff.provider.FHIRQuestionnaireResourceProvider;
import dk.s4.microservices.mobilepatientbff.provider.FHIRTaskResourceProvider;
import dk.s4.microservices.mobilepatientbff.security.MyAuthorizationInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.cors.CorsConfiguration;

import javax.servlet.ServletException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The Patient BFF is a "Backend-for-Frontend" service. This service is a
 * FHIR RESTful server without a database. It provides a REST API for patients.
 * <p>
 * This service collects and handles the business logic in a remote patient monitoring setting.
 * <p>
 * The responsibility of this service is to handle incoming REST requests, call the required backend microservices,
 * and then return the result to the calling client application.
 */
public class PatientBff extends RestfulServer {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(PatientBff.class);
    private static final int SOCKET_TIMEOUT_MILLIS = 60 * 1000;

    private static FhirContext fhirContext;

    private static UserContextResolverInterface contextResolver;
    private KafkaEventProducer kafkaCommandProducer;
    private ReplyEventProcessor replyEventProcessor;
    private KafkaConsumeAndProcess kafkaConsumeAndProcess;
    private Thread kafkaConsumeAndProcessThread;


    public PatientBff() throws KafkaInitializationException, MessagingInitializationException {

        super(getMyFhirContext());
        registerAndCheckEnvironmentVars();
        if (System.getenv("ENABLE_DIAS_AUTHENTICATION").equals("true")) {
            contextResolver = new DiasUserContextResolver(System.getenv("USER_CONTEXT_SERVICE_URL"));
        } else if (System.getenv("ENABLE_KEYCLOAK_GATEKEEPER_AUTHORIZATION").equals("true")) {
            contextResolver = new KeycloakGatekeeperUserContextResolver();
        } else if (System.getenv("ENABLE_OAUTH2_PROXY_AUTHORIZATION").equals("true")) {
            contextResolver = new OAuth2ProxyUserContextResolver();
        } else {
            contextResolver = new NullUserContextResolver(LOGGER);
        }

        if (ServiceVariables.ENABLE_KAFKA.isSetToTrue())
            initKafka();
    }

    /**
     * Singleton FhirContext
     *
     * @return the FhirContext
     */
    public static FhirContext getMyFhirContext() {
        if (fhirContext == null) {
            fhirContext = FhirContext.forR4();
            fhirContext.setParserErrorHandler(new StrictErrorHandler());
            fhirContext.getRestfulClientFactory().setSocketTimeout(SOCKET_TIMEOUT_MILLIS);
        }
        return fhirContext;
    }

    private void registerAndCheckEnvironmentVars() {
        ServiceVariables.registerAndEnsurePresence();
    }

    @Override
    public void destroy() {
        System.out.println("Shutting down employee-BFF");
        kafkaConsumeAndProcess.stopThread();
        try {
            kafkaConsumeAndProcessThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize() throws ServletException {
        super.initialize();

        LOGGER.debug("initialize");

        if (ServiceVariables.KEYCLOAK_CLIENT_NAME.getValue() == null) {
            LOGGER.warn(ServiceVariables.KEYCLOAK_CLIENT_NAME.getKey() + " is not defined");
        }

        /*
         * Enable CORS
         */
        CorsConfiguration config = new CorsConfiguration();
        CorsInterceptor corsInterceptor = new CorsInterceptor(config);
        config.addAllowedHeader("Origin");
        config.addAllowedHeader("Accept");
        config.addAllowedHeader("Prefer");
        config.addAllowedHeader("X-Requested-With");
        config.addAllowedHeader("Content-Type");
        config.addAllowedHeader("Access-Control-Request-Method");
        config.addAllowedHeader("Access-Control-Request-Headers");
        config.addAllowedHeader("Authorization");
        config.addAllowedHeader("SESSION");
        config.addAllowedOrigin("*");
        config.addExposedHeader("Location");
        config.addExposedHeader("Content-Location");
        config.setAllowedMethods(Arrays.asList("GET", "OPTIONS"));
        registerInterceptor(corsInterceptor);

        if (ServiceVariables.ENABLE_AUTH.isSetToTrue()) {
            LOGGER.debug("Initializing MyAuthorizationInterceptor");
            // Register the authorization interceptor
            MyAuthorizationInterceptor authzInterceptor = new MyAuthorizationInterceptor();
            this.registerInterceptor(authzInterceptor);
        }

        // Register adapter that handles metrics, correlation-ids, authentication etc.
        registerInterceptor(new MyInterceptorAdaptor(contextResolver));

        if (ServiceVariables.ENABLE_DIAS_AUTHENTICATION.isSetToTrue()) {
            DiasInterceptorAdaptor diasInterceptorAdaptor = new DiasInterceptorAdaptor(contextResolver);
            registerInterceptor(diasInterceptorAdaptor);
        }

        // Register HAPI FHIR's built in logging interceptor
        LoggingInterceptor loggingInterceptor = new LoggingInterceptor();
        registerInterceptor(loggingInterceptor);
        loggingInterceptor.setMessageFormat("operationType: ${operationType}\n"
                                                    + "operationName: ${operationName}\n"
                                                    + "idOrResourceName: ${idOrResourceName}\n"
                                                    + "requestParameters: ${requestParameters}\n"
                                                    + "requestUrl: ${requestUrl}\n"
                                                    + "requestVerb: ${requestVerb}\n"
                                                    + "processingTimeMillis: ${processingTimeMillis}"
                                           );
        loggingInterceptor.setErrorMessageFormat("ERROR: ${exceptionMessage}\n"
                                                         + "operationType: ${operationType}\n"
                                                         + "operationName: ${operationName}\n"
                                                         + "idOrResourceName: ${idOrResourceName}\n"
                                                         + "requestParameters: ${requestParameters}\n"
                                                         + "requestUrl: ${requestUrl}\n"
                                                         + "requestVerb: ${requestVerb}\n"
                                                         + "processingTimeMillis: ${processingTimeMillis}"										   );
        loggingInterceptor.setLogger(LOGGER);

        /*
         * Add resource providers
         */

        List<IResourceProvider> providers = new ArrayList<>();
        FhirContext context = getMyFhirContext();
        MyFhirClientFactory clientFactory = MyFhirClientFactory.getInstance();
        providers.add(new FHIRObservationResourceProvider(context,
                                                          contextResolver,
                                                          clientFactory));
        providers.add(new FHIRQuestionnaireResourceProvider(context,
                                                            contextResolver,
                                                            clientFactory));
        providers.add(new FHIRTaskResourceProvider(context,
                                                   contextResolver,
                                                   clientFactory,
                                                    kafkaCommandProducer,
                                                    replyEventProcessor));
        providers.add(new FHIRCarePlanResourceProvider(context,
                                                       contextResolver,
                                                       clientFactory));
        setResourceProviders(providers);

        /*
         * This server interceptor causes the server to return nicely formatter
         * and coloured responses instead of plain JSON/XML if the request is
         * coming from a browser window. It is optional, but can be nice for
         * testing.
         */
        registerInterceptor(new ResponseHighlighterInterceptor());

        /*
         * Tells the server to return pretty-printed responses by default
         */
        setDefaultPrettyPrint(true);
        setDefaultResponseEncoding(EncodingEnum.JSON);

        LOGGER.info("Patient BFF initialized");

        if (ServiceVariables.DIAS_AUDIT_ENABLED.isSetToTrue()){
            registerInterceptor(new DiasAuditLogInterceptorAdaptor(contextResolver,
                    new DiasSender(ServiceVariables.DIAS_AUDIT_URL.getValue(), ServiceVariables.SERVICE_NAME.getValue())));
        }
    }

    private void initKafka() throws KafkaInitializationException, MessagingInitializationException {
        //Create producer for asynchonous invocations of create and update commands
        kafkaCommandProducer = new KafkaEventProducer(ServiceVariables.SERVICE_NAME.getValue());
        if (ServiceVariables.ENABLE_DIAS_AUTHENTICATION.isSetToTrue())
            kafkaCommandProducer.setInterceptors(new DiasEventProducerInterceptor());

        List<Topic> replyTopics = new ArrayList<>();
        replyTopics.add(FhirTopics.dataCreated("Task"));
        replyTopics.add(FhirTopics.dataUpdated("Task"));
        replyTopics.add(FhirTopics.processingFailed("Task"));

        replyEventProcessor = new ReplyEventProcessor();
        kafkaConsumeAndProcess = new KafkaConsumeAndProcess(replyTopics, null, replyEventProcessor);
        kafkaConsumeAndProcessThread = new Thread(kafkaConsumeAndProcess, "KafkaConsumeAndProcessThread");
        kafkaConsumeAndProcessThread.start();
        HealthEndpoint.registerKafkaConsumeAndProcess(kafkaConsumeAndProcess);
    }
}
