package dk.s4.microservices.mobilepatientbff;

import dk.s4.microservices.microservicecommon.Env;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Enumeration of all environment variables, this service uses. Some variables are required while others are
 * optional.
 * This class ensures consistent behavior when accessing these variables, and convenience for validation and
 * access.
 */
public enum ServiceVariables {
    SERVICE_NAME("SERVICE_NAME", true),
    FHIR_VERSION("FHIR_VERSION", true),
    CORRELATION_ID("CORRELATION_ID", true),
    TRANSACTION_ID("TRANSACTION_ID", true),
    ENABLE_KAFKA("ENABLE_KAFKA",true),
    OUTCOME_SERVICE_URL("OUTCOME_SERVICE_URL", true),
    OUTCOMEDEFINITION_SERVICE_URL("OUTCOMEDEFINITION_SERVICE_URL", true),
    PATIENTCARE_SERVICE_URL("PATIENTCARE_SERVICE_URL", true),
    OFFICIAL_TASK_IDENTIFIER_SYSTEM("OFFICIAL_TASK_IDENTIFIER_SYSTEM", true),
    OFFICIAL_PATIENT_IDENTIFIER_SYSTEM("OFFICIAL_PATIENT_IDENTIFIER_SYSTEM", true),
    ENABLE_AUTH("ENABLE_AUTH", true),
    KEYCLOAK_CLIENT_NAME("KEYCLOAK_CLIENT_NAME", false),
    ENABLE_AUTHENTICATION("ENABLE_AUTHENTICATION", false),
    ENABLE_DIAS_AUTHENTICATION("ENABLE_DIAS_AUTHENTICATION", true),
    ENABLE_KEYCLOAK_GATEKEEPER_AUTHORIZATION("ENABLE_KEYCLOAK_GATEKEEPER_AUTHORIZATION", true),
    ENABLE_OAUTH2_PROXY_AUTHORIZATION("ENABLE_OAUTH2_PROXY_AUTHORIZATION", true),
    USER_CONTEXT_SERVICE_URL("USER_CONTEXT_SERVICE_URL", false),
    DIAS_AUDIT_URL("DIAS_AUDIT_URL", false),
    DIAS_AUDIT_ENABLED("DIAS_AUDIT_ENABLED", false);

    private String key;
    private boolean required;

    ServiceVariables(String key, boolean required) {
        this.key = key;
        this.required = required;
    }

    public static void registerAndEnsurePresence() {
        List<String> requiredKeys = variablesWith(v -> v.required).stream().map(ServiceVariables::getKey).collect(Collectors.toList());
        Env.registerRequiredEnvVars(requiredKeys);
        Env.checkEnv();
    }

    private static List<ServiceVariables> variablesWith(Predicate<ServiceVariables> predicate) {
        return Arrays.stream(ServiceVariables.values()).filter(predicate).collect(Collectors.toList());
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        String value = System.getenv(getKey());
        if (value == null || value.isEmpty()) {
            if (required) {
                throw new IllegalArgumentException("Required key not present: " + getKey());
            } else {
                return null;
            }
        }
        return value;
    }

    public boolean isSetToTrue() {
        String value = getValue();
        if (value == null) {
            return false;
        }
        return value.equalsIgnoreCase("true");
    }
}
