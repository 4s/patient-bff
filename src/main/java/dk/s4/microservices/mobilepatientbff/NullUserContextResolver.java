package dk.s4.microservices.mobilepatientbff;

import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import org.hl7.fhir.r4.model.HumanName;
import org.hl7.fhir.r4.model.Identifier;
import org.slf4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class NullUserContextResolver implements UserContextResolverInterface {

    private final Logger logger;

    public NullUserContextResolver(Logger logger){
        this.logger = logger;
    }

    @Override
    public String getUserId(HttpServletRequest httpServletRequest) throws UserContextResolverException {
        return null;
    }

    @Override
    public Identifier getFHIRUserId(HttpServletRequest request) throws UserContextResolverException {
        logger.warn("Patient-Bff is using mocking user context. To use Dias user context, enable Dias authentication in Patient-Bff.");
        return new DefaultIdentifier();
    }

    @Override
    public String getNameOfUser(HttpServletRequest request) throws UserContextResolverException {
        return null;
    }

    @Override
    public HumanName getFHIRNameOfUser(HttpServletRequest request) throws UserContextResolverException {
        return null;
    }

    @Override
    public String getSecurityToken(HttpServletRequest httpServletRequest) throws UserContextResolverException {
        return null;
    }

    @Override
    public String getUserOrganization(HttpServletRequest httpServletRequest) throws UserContextResolverException {
        return null;
    }

    @Override
    public Identifier getFHIRUserOrganization(HttpServletRequest httpServletRequest) throws UserContextResolverException {
        return null;
    }
}
