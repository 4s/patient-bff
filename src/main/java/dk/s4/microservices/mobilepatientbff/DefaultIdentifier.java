package dk.s4.microservices.mobilepatientbff;

import org.hl7.fhir.r4.model.Identifier;

public final class DefaultIdentifier extends Identifier {
    private static final String nancyTestUser = "3103979995";

    public DefaultIdentifier() {
        setValue(nancyTestUser);
    }
}
