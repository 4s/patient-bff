package dk.s4.microservices.mobilepatientbff.test;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.client.api.*;
import ca.uhn.fhir.rest.client.interceptor.LoggingInterceptor;
import ca.uhn.fhir.rest.gclient.IQuery;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.mobilepatientbff.servlet.PatientBff;
import org.eclipse.jetty.server.Server;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Patient;
import org.junit.*;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Unit and integration tests for the Patient BFF service
 */
public class PatientBffTest {

    private static IGenericClient ourClient;
    private static FhirContext ourCtx = PatientBff.getMyFhirContext();
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(PatientBffTest.class);

    private static int ourPort;
    private static Server ourServer;
    private static String ourServerBase;

    private static IParser jsonParser;
    private static String ENVIRONMENT_FILE = "service.env";

    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Ignore
    @Test
    public void testGetObservations() {
        logger.debug("testGetObservations");

        String searchUrl = ourServerBase + "/Observation?";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }

    @Ignore
    @Test
    public void testGetQuestionnaires() {
        logger.debug("testGetQuestionnaires");

        String searchUrl = ourServerBase + "/Questionnaire?";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);
        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }

    @Ignore
    @Test
    public void testGetFails() throws IOException {
        logger.debug("testGetFails");

        try {
            // Wrong resource type here: Patient...
            Bundle result = ourClient.search().forResource(Patient.class).returnBundle(Bundle.class).execute();

        } catch (ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException e) {
            Assert.assertEquals("HTTP 404 Not Found: Unknown resource type 'Patient' - Server knows how to handle: [Task, StructureDefinition, Questionnaire, Observation, OperationDefinition]",
                    e.getMessage());
        }
    }

    @AfterClass
    public static void afterClass() throws Exception {
        ourServer.stop();
    }

    @BeforeClass
    public static void beforeClass() throws Exception {

        jsonParser = ourCtx.newJsonParser();
        jsonParser.setPrettyPrint(true);

        /*
         * This runs under maven, and I'm not sure how else to figure out the target directory from code..
         */
        String path = PatientBffTest.class.getClassLoader().getResource(".keep_patientbff_service").getPath();
        path = new File(path).getParent();
        path = new File(path).getParent();
        path = new File(path).getParent();

        logger.info("Project base path is: {}", path);

        // Read and set environment variables from .env file:
        TestUtils.readEnvironment(path + "/" + ENVIRONMENT_FILE, environmentVariables);

        //Init web server and start it
        ourPort = TestUtils.findFreePort();
        ourServer = new Server(ourPort);
        ourServerBase = TestUtils.initWebServer(path+"/src/main/webapp/WEB-INF/without-keycloak/web.xml",path+"/target/service", environmentVariables, ourPort, ourServer);


        ourCtx.getRestfulClientFactory().setServerValidationMode(ServerValidationModeEnum.NEVER);
        ourCtx.getRestfulClientFactory().setSocketTimeout(1200 * 1000);

        ourClient = ourCtx.newRestfulGenericClient(ourServerBase);
        ourClient.registerInterceptor(new LoggingInterceptor(true));

    }
}
