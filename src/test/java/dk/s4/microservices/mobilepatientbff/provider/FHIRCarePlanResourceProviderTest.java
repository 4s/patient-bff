package dk.s4.microservices.mobilepatientbff.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.gclient.IQuery;
import ca.uhn.fhir.rest.gclient.IUntypedQuery;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import dk.s4.microservices.mobilepatientbff.ServiceVariables;
import org.hl7.fhir.instance.model.api.IBaseBundle;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Questionnaire;
import org.junit.Before;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FHIRCarePlanResourceProviderTest {

    private FhirContext context;
    private UserContextResolverInterface userContextResolver;
    private MyFhirClientFactory clientFactory;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private List<String> clientUrls;
    private List<String> searchUrls;
    private Questionnaire questionnaire;
    private static final String USERID = "user1234";
    private static final String USERSYSTEM = "mySystem";

    @Before
    public void setUp() throws Exception {
        clientUrls = new ArrayList<>();
        searchUrls = new ArrayList<>();

        TestUtils.readEnvironment("service.env", new EnvironmentVariables());
        TestUtils.readEnvironment("deploy.env", new EnvironmentVariables());

        context = mock(FhirContext.class);
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        userContextResolver = mock(UserContextResolverInterface.class);
        when(userContextResolver.getFHIRUserId(request)).thenReturn(new Identifier().setValue(USERID).setSystem(USERSYSTEM));

        IGenericClient client = mock(IGenericClient.class);

        @SuppressWarnings("unchecked")
        IUntypedQuery<IBaseBundle> search = (IUntypedQuery<IBaseBundle>) mock(IUntypedQuery.class);
        IQuery query = mock(IQuery.class);

        when(query.returnBundle(Bundle.class)).thenReturn(query);

        when(search.byUrl(anyString())).thenAnswer((invocation) -> {
            searchUrls.add((String) invocation.getArguments()[0]);
            return query;
        });

        when(client.search()).thenReturn(search);

        clientFactory = mock(MyFhirClientFactory.class);
        when(clientFactory.getClientFromBaseUrl(anyString(), any(FhirContext.class)))
                .thenAnswer((invocation) -> {
                    clientUrls.add((String) invocation.getArguments()[0]);
                    return client;
                });
    }

    @Test
    public void getMyActive() {
        FHIRCarePlanResourceProvider provider = new FHIRCarePlanResourceProvider(context,userContextResolver,clientFactory);
        provider.getMyActive(request,response);

        assertThat(clientUrls).hasSize(1);
        assertThat(searchUrls).hasSize(1);
        assertThat(clientUrls.get(0)).isEqualTo(ServiceVariables.PATIENTCARE_SERVICE_URL.getValue());
        assertThat(searchUrls.get(0)).contains("getActiveForPatient");
        assertThat(searchUrls.get(0)).contains("identifier="
                + ServiceVariables.OFFICIAL_PATIENT_IDENTIFIER_SYSTEM.getValue()
                + "|"
                + USERID);

    }
}