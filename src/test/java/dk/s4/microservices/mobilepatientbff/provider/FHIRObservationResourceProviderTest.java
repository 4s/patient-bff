package dk.s4.microservices.mobilepatientbff.provider;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.api.IRestfulClientFactory;
import ca.uhn.fhir.rest.gclient.IQuery;
import ca.uhn.fhir.rest.gclient.IUntypedQuery;
import ca.uhn.fhir.rest.param.DateRangeParam;
import ca.uhn.fhir.rest.param.TokenParam;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.ThreadLocalContext;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import dk.s4.microservices.mobilepatientbff.ServiceVariables;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hl7.fhir.instance.model.api.IBaseBundle;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Observation;
import org.junit.Before;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.mockito.Mockito;

public class FHIRObservationResourceProviderTest {
    private FhirContext context;
    private UserContextResolverInterface userContextResolver;
    private MyFhirClientFactory clientFactory;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private List<String> clientUrls;
    private List<String> searchUrls;
    private static final String USERID = "user1234";
    private static final String USERSYSTEM = "mySystem";

    @Before
    public void setUp() throws Exception {
        clientUrls = new ArrayList<>();
        searchUrls = new ArrayList<>();

        TestUtils.readEnvironment("service.env", new EnvironmentVariables());
        TestUtils.readEnvironment("deploy.env", new EnvironmentVariables());

        context = mock(FhirContext.class);
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        userContextResolver = mock(UserContextResolverInterface.class);
        when(userContextResolver.getFHIRUserId(request)).thenReturn(new Identifier().setValue(USERID).setSystem(USERSYSTEM));

        IGenericClient client = mock(IGenericClient.class);

        @SuppressWarnings("unchecked")
        IUntypedQuery<IBaseBundle> search = (IUntypedQuery<IBaseBundle>) mock(IUntypedQuery.class);
        IQuery query = mock(IQuery.class);

        when(query.returnBundle(Bundle.class)).thenReturn(query);

        when(search.byUrl(anyString())).thenAnswer((invocation) -> {
            searchUrls.add((String) invocation.getArguments()[0]);
            return query;
        });

        when(client.search()).thenReturn(search);

        clientFactory = mock(MyFhirClientFactory.class);
        when(clientFactory.getClientFromBaseUrl(anyString(), any())).thenAnswer((invocation) -> {
            clientUrls.add((String) invocation.getArguments()[0]);
            return client;
        });
    }

    @Test
    public void endPointRedirectsAsExpected() {
        FHIRObservationResourceProvider provider = new FHIRObservationResourceProvider(context, userContextResolver, clientFactory);
        provider.getMyObservations(request, response, null, null);
        assertThat(clientUrls).hasSize(1);
        assertThat(searchUrls).hasSize(1);
        assertThat(clientUrls.get(0)).isEqualTo(getOutcomeServiceUrl());
        assertThat(searchUrls.get(0)).contains("searchBySubjectIdentifier");
        assertThat(searchUrls.get(0)).contains("subject-identifier="
                + ServiceVariables.OFFICIAL_PATIENT_IDENTIFIER_SYSTEM.getValue()
                + "|"
                + USERID);
    }

    @Test
    public void dateIncludedInRedirect() {
        FHIRObservationResourceProvider provider = new FHIRObservationResourceProvider(context, userContextResolver, clientFactory);
        DateRangeParam date = new DateRangeParam();
        date.setRangeFromDatesInclusive("1000-11-11", "1111-11-11");
        provider.getMyObservations(request, response, date, null);
        assertThat(clientUrls).hasSize(1);
        assertThat(searchUrls).hasSize(1);
        assertThat(searchUrls.get(0)).contains(Observation.SP_DATE + "=ge1000-11-11");
        assertThat(searchUrls.get(0)).contains(Observation.SP_DATE + "=le1111-11-11");
    }

    @Test
    public void singleDateIncludedInRedirect() {
        FHIRObservationResourceProvider provider = new FHIRObservationResourceProvider(context, userContextResolver, clientFactory);
        DateRangeParam date = new DateRangeParam();
        date.setLowerBound("1999-01-01");
        provider.getMyObservations(request, response, date, null);
        assertThat(clientUrls).hasSize(1);
        assertThat(searchUrls).hasSize(1);
        assertThat(searchUrls.get(0)).contains(Observation.SP_DATE + "=ge1999-01-01");
    }

    @Test
    public void typeIncludedInRedirect() {
        FHIRObservationResourceProvider provider = new FHIRObservationResourceProvider(context, userContextResolver, clientFactory);
        TokenParam tokenParam = new TokenParam().setValue("MyCodeValue");
        provider.getMyObservations(request, response, null, tokenParam);
        assertThat(clientUrls).hasSize(1);
        assertThat(searchUrls).hasSize(1);
        assertThat(searchUrls.get(0)).contains(Observation.SP_CODE + "=MyCodeValue");
    }

    @Test
    public void typeWithSystemIncludedInRedirect() {
        FHIRObservationResourceProvider provider = new FHIRObservationResourceProvider(context, userContextResolver, clientFactory);
        TokenParam tokenParam = new TokenParam().setValue("MyCodeValue").setSystem("MySystem");
        provider.getMyObservations(request, response, null, tokenParam);
        assertThat(clientUrls).hasSize(1);
        assertThat(searchUrls).hasSize(1);
        assertThat(searchUrls.get(0)).contains(Observation.SP_CODE + "=MySystem|MyCodeValue");
    }

    private String getOutcomeServiceUrl() {
        return ServiceVariables.OUTCOME_SERVICE_URL.getValue();
    }
}