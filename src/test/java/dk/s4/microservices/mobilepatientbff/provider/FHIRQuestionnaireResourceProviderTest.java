package dk.s4.microservices.mobilepatientbff.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.gclient.IQuery;
import ca.uhn.fhir.rest.gclient.IUntypedQuery;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.fhir.ResourceUtil;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import dk.s4.microservices.mobilepatientbff.ServiceVariables;
import dk.s4.microservices.mobilepatientbff.servlet.PatientBff;
import org.hl7.fhir.instance.model.api.IBaseBundle;
import org.hl7.fhir.r4.model.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class FHIRQuestionnaireResourceProviderTest {
    private FhirContext context;
    private UserContextResolverInterface userContextResolver;
    private MyFhirClientFactory clientFactory;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private List<String> clientUrls;
    private List<String> searchUrls;
    private Questionnaire questionnaire;
    private static final String USERID = "user1234";
    private static final String USERSYSTEM = "mySystem";
    private static final String QUESTIONNAIREID = "q1234";

    @Before
    public void setUp() throws Exception {
        clientUrls = new ArrayList<>();
        searchUrls = new ArrayList<>();

        TestUtils.readEnvironment("service.env", new EnvironmentVariables());
        TestUtils.readEnvironment("deploy.env", new EnvironmentVariables());

        context = mock(FhirContext.class);
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        userContextResolver = mock(UserContextResolverInterface.class);
        when(userContextResolver.getFHIRUserId(request)).thenReturn(new Identifier().setValue(USERID).setSystem(USERSYSTEM));

        IGenericClient client = mock(IGenericClient.class);

        @SuppressWarnings("unchecked")
        IUntypedQuery<IBaseBundle> search = (IUntypedQuery<IBaseBundle>) mock(IUntypedQuery.class);
        IQuery query = mock(IQuery.class);

        when(query.returnBundle(Bundle.class)).thenReturn(query);

        when(search.byUrl(anyString())).thenAnswer((invocation) -> {
            searchUrls.add((String) invocation.getArguments()[0]);
            return query;
        });

        when(client.search()).thenReturn(search);

        String resourceString = ResourceUtil.stringFromResource("FHIR-clean/CarePlan.json");
        CarePlan carePlan = (CarePlan) PatientBff.getMyFhirContext().newJsonParser().parseResource(resourceString);
        Bundle cpBundle = new Bundle();
        cpBundle.addEntry().setResource(carePlan);

        resourceString = ResourceUtil.stringFromResource("FHIR-clean/Questionnaire.json");
        questionnaire = (Questionnaire) PatientBff.getMyFhirContext().newJsonParser().parseResource(resourceString);
        questionnaire.setId(QUESTIONNAIREID);
        Bundle questBundle = new Bundle();
        questBundle.addEntry().setResource(questionnaire);

        when(query.execute()).thenAnswer((invocation) -> {
            if (clientUrls.size() == 1)
                return cpBundle;
            else
                return questBundle;
        });

        clientFactory = mock(MyFhirClientFactory.class);
        when(clientFactory.getClientFromBaseUrl(anyString(), any(FhirContext.class)))
                .thenAnswer((invocation) -> {
                    clientUrls.add((String) invocation.getArguments()[0]);
                    return client;
                });
    }

    @Test
    public void endPointRedirectsAsExpected() {
        FHIRQuestionnaireResourceProvider provider = new FHIRQuestionnaireResourceProvider(context, userContextResolver, clientFactory);
        Bundle result = provider.getMyQuestionnaires(request, response);
        assertThat(clientUrls).hasSize(2);
        assertThat(searchUrls).hasSize(2);
        assertThat(clientUrls.get(0)).isEqualTo(ServiceVariables.PATIENTCARE_SERVICE_URL.getValue());
        assertThat(clientUrls.get(1)).isEqualTo(ServiceVariables.OUTCOMEDEFINITION_SERVICE_URL.getValue());
        assertThat(searchUrls.get(0)).contains("getActiveForPatient");
        assertThat(searchUrls.get(0)).contains("identifier="
                + ServiceVariables.OFFICIAL_PATIENT_IDENTIFIER_SYSTEM.getValue()
                + "|"
                + USERID);

        //URL is expected to look similar to this: http://outcomedefinition-service:8080/baseR4/Questionnaire?url=canonical://uuid/Questionnaire/f14997cf-e16e-4e4b-8b3a-1f419386f597&version=1.0.0
        assertThat(searchUrls.get(1)).contains("/Questionnaire?");
        assertThat(searchUrls.get(1)).contains("url="
                + questionnaire.getUrl() + "&version=" + questionnaire.getVersion());
        assertThat(result.getEntry().size() == 1);
        assertThat(result.getEntry().get(0).getResource().getId().equals(questionnaire.getId()));
    }

    @Test
    public void getCarePlanForYieldExpectingUrls() {
        FHIRQuestionnaireResourceProvider provider = new FHIRQuestionnaireResourceProvider(context, userContextResolver, clientFactory);
        TokenParam tokenParam = new TokenParam().setSystem("system").setValue("value");
        Bundle result = provider.getForCarePlan(request, response, tokenParam);

        assertThat(clientUrls).hasSize(2);
        assertThat(searchUrls).hasSize(2);
        assertThat(clientUrls.get(0)).isEqualTo(ServiceVariables.PATIENTCARE_SERVICE_URL.getValue());
        assertThat(clientUrls.get(1)).isEqualTo(ServiceVariables.OUTCOMEDEFINITION_SERVICE_URL.getValue());
        assertThat(searchUrls.get(0)).contains("/CarePlan?");
        assertThat(searchUrls.get(0)).contains("identifier="
                + "system"
                + "|"
                + "value");

        //URL is expected to look similar to this:
        assertThat(searchUrls.get(1)).contains("/Questionnaire?");
        assertThat(searchUrls.get(1)).contains("url="
                + questionnaire.getUrl() + "&version=" + questionnaire.getVersion());
        assertThat(result.getEntry().size() == 1);
        assertThat(result.getEntry().get(0).getResource().getId().equals(questionnaire.getId()));


    }

    @Test(expected = ResourceNotFoundException.class)
    public void extractQuestionnairesToBundleThrowsExceptionIfNoActivity() {
        FHIRQuestionnaireResourceProvider provider = new FHIRQuestionnaireResourceProvider(context, userContextResolver, clientFactory);
        CarePlan activeCarePlan = new CarePlan();
        Bundle carePlanBundle = new Bundle();
        carePlanBundle.addEntry().setResource(activeCarePlan);

        provider.extractQuestionnairesToBundle(carePlanBundle);
    }

    @Test
    public void extractQuestionnairesToBundleExtractsCorrectlyOneCarePlan() throws IOException {
        IParser jsonParser = PatientBff.getMyFhirContext().newJsonParser();
        clientUrls.add("something");

        FHIRQuestionnaireResourceProvider provider = new FHIRQuestionnaireResourceProvider(context, userContextResolver, clientFactory);
        String resourceString = ResourceUtil.stringFromResource("FHIR-clean/CarePlan.json");
        CarePlan activeCarePlan = jsonParser.parseResource(CarePlan.class, resourceString);
        Bundle carePlanBundle = new Bundle();
        carePlanBundle.addEntry().setResource(activeCarePlan);

        Bundle bundle = provider.extractQuestionnairesToBundle(carePlanBundle);

        assertThat(bundle.getEntry().size()).isNotEqualTo(0);
        assertThat(bundle.getEntry().size()).isEqualTo(1);
    }

    @Test
    public void extractQuestionnairesToBundleExtractsCorrectlyThreeCarePlan() throws IOException {
        IParser jsonParser = PatientBff.getMyFhirContext().newJsonParser();
        clientUrls.add("something");

        FHIRQuestionnaireResourceProvider provider = new FHIRQuestionnaireResourceProvider(context, userContextResolver, clientFactory);
        String resourceString = ResourceUtil.stringFromResource("FHIR-clean/CarePlan.json");
        CarePlan activeCarePlan = jsonParser.parseResource(CarePlan.class, resourceString);
        Bundle carePlanBundle = new Bundle();
        carePlanBundle.addEntry().setResource(activeCarePlan);
        carePlanBundle.addEntry().setResource(activeCarePlan);
        carePlanBundle.addEntry().setResource(activeCarePlan);

        Bundle bundle = provider.extractQuestionnairesToBundle(carePlanBundle);

        assertThat(bundle.getEntry().size()).isNotEqualTo(0);
        assertThat(bundle.getEntry().size()).isEqualTo(3);
    }
}