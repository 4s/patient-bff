package dk.s4.microservices.mobilepatientbff.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.gclient.IQuery;
import ca.uhn.fhir.rest.gclient.IUntypedQuery;
import ca.uhn.fhir.rest.param.DateRangeParam;
import ca.uhn.fhir.rest.param.TokenParam;
import dk.s4.microservices.messaging.ReplyEventProcessor;
import dk.s4.microservices.messaging.kafka.KafkaEventProducer;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import dk.s4.microservices.mobilepatientbff.ServiceVariables;
import org.hl7.fhir.instance.model.api.IBaseBundle;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Task;
import org.junit.Before;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class FHIRTaskResourceProviderTest {
    private FhirContext context;
    private KafkaEventProducer eventProducer;
    private ReplyEventProcessor eventProcessor;
    private UserContextResolverInterface userContextResolver;
    private MyFhirClientFactory clientFactory;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private List<String> clientUrls;
    private List<String> searchUrls;
    private static final String USERID = "user1234";
    private static final String USERSYSTEM = "mySystem";
    private static final String SESSIONID = "session1234";

    @Before
    public void setUp() throws Exception {
        clientUrls = new ArrayList<>();
        searchUrls = new ArrayList<>();


        EnvironmentVariables variables = new EnvironmentVariables();
        TestUtils.readEnvironment("deploy.env", variables);
        TestUtils.readEnvironment("service.env", variables);

        context = mock(FhirContext.class);
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        eventProducer = mock(KafkaEventProducer.class);
        eventProcessor = mock(ReplyEventProcessor.class);
        userContextResolver = mock(UserContextResolverInterface.class);
        when(userContextResolver.getFHIRUserId(request)).thenReturn(new Identifier().setValue(USERID).setSystem(USERSYSTEM));

        IGenericClient client = mock(IGenericClient.class);

        @SuppressWarnings("unchecked")
        IUntypedQuery<IBaseBundle> search = (IUntypedQuery<IBaseBundle>) mock(IUntypedQuery.class);
        IQuery query = mock(IQuery.class);

        when(query.returnBundle(Bundle.class)).thenReturn(query);

        when(search.byUrl(anyString())).thenAnswer((invocation) -> {
            searchUrls.add((String) invocation.getArguments()[0]);
            return query;
        });

        when(client.search()).thenReturn(search);

        clientFactory = mock(MyFhirClientFactory.class);
        when(clientFactory.getClientFromBaseUrl(anyString(), any())).thenAnswer((invocation) -> {
            clientUrls.add((String) invocation.getArguments()[0]);
            return client;
        });
    }

    @Test
    public void getTaskForPatientWithCodeAndDate() {
        FHIRTaskResourceProvider provider = new FHIRTaskResourceProvider(context, userContextResolver, clientFactory, eventProducer, eventProcessor);
        TokenParam code = new TokenParam().setValue("review");
        DateRangeParam date = new DateRangeParam();
        date.setRangeFromDatesInclusive("1000-11-11", "1111-11-11");
        provider.getTaskForPatientWithCode(request, response, code, date);
        assertThat(clientUrls).hasSize(1);
        assertThat(searchUrls).hasSize(1);
        assertThat(clientUrls.get(0)).isEqualTo(ServiceVariables.OUTCOME_SERVICE_URL.getValue());
        assertThat(searchUrls.get(0)).contains("getTaskForPatientWithCode");
        assertThat(searchUrls.get(0)).contains("code=review");
        assertThat(searchUrls.get(0)).contains("identifier="
                + ServiceVariables.OFFICIAL_PATIENT_IDENTIFIER_SYSTEM.getValue()
                + "|"
                + USERID);
        assertThat(searchUrls.get(0)).contains(Task.SP_AUTHORED_ON + "=ge1000-11-11");
        assertThat(searchUrls.get(0)).contains(Task.SP_AUTHORED_ON + "=le1111-11-11");
    }

    @Test
    public void getAcknowledgementTasksWithDate() {
        DateRangeParam date = new DateRangeParam();
        date.setRangeFromDatesInclusive("1000-11-11", "1111-11-11");
        FHIRTaskResourceProvider provider = new FHIRTaskResourceProvider(context, userContextResolver, clientFactory, eventProducer, eventProcessor);
        provider.getAcknowledgementTasks(request, response, date);
        assertThat(clientUrls).hasSize(1);
        assertThat(searchUrls).hasSize(1);
        assertThat(clientUrls.get(0)).isEqualTo(ServiceVariables.OUTCOME_SERVICE_URL.getValue());
        assertThat(searchUrls.get(0)).contains("getTaskForPatientWithCode");
        assertThat(searchUrls.get(0)).contains("code=https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100794407/Task%2btype%2bcoding%2bsystem|acknowledgement");
        assertThat(searchUrls.get(0)).contains("identifier="
                + ServiceVariables.OFFICIAL_PATIENT_IDENTIFIER_SYSTEM.getValue()
                + "|"
                + USERID);
        assertThat(searchUrls.get(0)).contains(Task.SP_AUTHORED_ON + "=ge1000-11-11");
        assertThat(searchUrls.get(0)).contains(Task.SP_AUTHORED_ON + "=le1111-11-11");
    }
}