package dk.s4.microservices.mobilepatientbff;

import org.junit.Before;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class ServiceVariablesTest {
    private EnvironmentVariables variables;

    @Before
    public void setup() {
        variables = new EnvironmentVariables();
    }

    @Test
    public void requestingNonExistingRequiredVariableThrowsException() {
        Throwable throwable = catchThrowable(ServiceVariables.ENABLE_AUTH::getValue);
        assertThat(throwable).isNotNull();
        assertThat(throwable).isInstanceOf(IllegalArgumentException.class);
        assertThat(throwable).hasMessageContaining(ServiceVariables.ENABLE_AUTH.getKey());
        assertThat(throwable).hasMessageContaining("Required key");
    }

    @Test
    public void requestingNonExistingOptionalVariableReturnsNull() {
        String optionalValue = ServiceVariables.KEYCLOAK_CLIENT_NAME.getValue();
        assertThat(optionalValue).isNull();
    }

    @Test
    public void existingVariablesCanBeRead() {
        variables.set(ServiceVariables.ENABLE_AUTH.getKey(), "test");
        assertThat(ServiceVariables.ENABLE_AUTH.getValue()).isEqualTo("test");
        variables.clear(ServiceVariables.ENABLE_AUTH.getKey());
    }

    @Test
    public void testIsSetToTrueReturnsFalseWithTestValue() {
        variables.set(ServiceVariables.ENABLE_AUTH.getKey(), "test");
        assertThat(ServiceVariables.ENABLE_AUTH.isSetToTrue()).isFalse();
        variables.clear(ServiceVariables.ENABLE_AUTH.getKey());
    }

    @Test
    public void testIsSetToTrueReturnsFalseWhenNotSetWithOptionalKey() {
        assertThat(ServiceVariables.KEYCLOAK_CLIENT_NAME.isSetToTrue()).isFalse();
    }

    @Test
    public void testIsSetToTrueReturnsFailsWhenNotSetWithRequiredKey() {
        Throwable throwable = catchThrowable(ServiceVariables.ENABLE_AUTH::getValue);
        assertThat(throwable).isNotNull();
    }

    @Test
    public void testIsSetToTrueReturnsTrueWhenTrue() {
        variables.set(ServiceVariables.ENABLE_AUTH.getKey(), "true");
        assertThat(ServiceVariables.ENABLE_AUTH.isSetToTrue()).isTrue();
        variables.clear(ServiceVariables.ENABLE_AUTH.getKey());
    }
}